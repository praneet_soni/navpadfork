function [state, history] = forward_sim_trajectory( old_state, history, chosen_path, planner_params, display_obj )
%FORWARD_SIM_TRAJECTORY Summary of this function goes here
%   Detailed explanation goes here

% Update state and history
state = struct(chosen_path);
history_increment = struct(chosen_path);
for field = fieldnames(state)'
    state.(field{1}) = chosen_path.(field{1})(planner_params.ind);
    history_increment.(field{1}) = chosen_path.(field{1})(1:planner_params.ind);
end

history = [history history_increment];
display_obj.handle_set.chosen = plot_path( chosen_path, display_obj.handle_set.chosen);
display_obj.handle_set.history = plot_history( history, display_obj.handle_set.history);
display_obj.handle_set.world_chosen = plot_path( chosen_path, display_obj.handle_set.world_chosen);
display_obj.handle_set.world_history = plot_history( history, display_obj.handle_set.world_history);

%Video recording
if (display_obj.record_video)
    writeVideo(display_obj.vidObj1, getframe(display_obj.handle_set.fig_main));
    writeVideo(display_obj.vidObj2, getframe(display_obj.handle_set.fig_heur));
end

end

