function [ validPaths ] = queryVelocityFeasability2(pose,map,stateMatrix,queryState,minState,stateResolution,minX,minY,mapRes,globalPathList,globalPathId)

vId = value2Id(queryState,minState,stateResolution);
qPathIds = stateMatrix(vId(1),vId(2),vId(3)).pathIds;
validPaths = [];

for i=1:size(qPathIds,1);
    pId = globalPathId(qPathIds(i),:);
    path = [globalPathList(pId(1):pId(2),2) globalPathList(pId(1):pId(2),3)];
    path = transformPoint2Global(path,pose(3),pose(1:2));
    pathCoord = coord2index(path(:,1),path(:,2),mapRes,minX,minY);
    if isempty(pathCoord(pathCoord(:,1)<1,:)) && isempty(pathCoord(pathCoord(:,1)>size(map,1),:))...
            && isempty(pathCoord(pathCoord(:,2)<1,:)) && isempty(pathCoord(pathCoord(:,2)>size(map,2),:))
        indices = sub2ind(size(map),pathCoord(:,1),pathCoord(:,2));
        if size(map(map(indices)<0.7),1)<=0
            validPaths = [validPaths;qPathIds(i)];
        end
    end
end

end



